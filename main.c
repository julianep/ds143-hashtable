#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct grupo {
    int senha;
    char nome[30];
    int v1, v2;
};

typedef struct hash {
    int qtd, TABLE_SIZE;
    struct grupo **itens;
} Hash;

Hash *criaHash(int TABLE_SIZE) {
    Hash *a = (Hash *) malloc(sizeof(Hash));
    if (a != NULL) {
        int i;
        a->TABLE_SIZE = TABLE_SIZE;
        a->itens = (struct grupo **)
                malloc(TABLE_SIZE * sizeof(struct grupo));
        if (a->itens == NULL) {
            free(a);
            return NULL;
        }
        a->qtd = 0;
        for (i = 0; i < a->TABLE_SIZE; i++)
            a->itens[i] = NULL;
    }
    return a;
}

int divChave(int chave, int TABLE_SIZE) {
    return (chave & 0x7FFFFFFF) % TABLE_SIZE;
}

int buscaHash(Hash *a, int m, struct grupo *gr) {
    if (a == NULL)
        return 0;

    int nxt = divChave(m, a->TABLE_SIZE);
    if (a->itens[nxt] == NULL)
        return 0;
    *gr = *(a->itens[nxt]);
    return 1;
}


int insereHash(Hash *a, struct grupo gr) {
    if (a == NULL || a->qtd == a->TABLE_SIZE)
        return 0;

    int chave = gr.senha;
    int nxt = divChave(chave, a->TABLE_SIZE);
    struct grupo *novo;
    novo = (struct grupo *) malloc(sizeof(struct grupo));
    if (novo == NULL)
        return 0;
    *novo = gr;
    a->itens[nxt] = novo;
    a->qtd++;
    return 1;
}

void removeHash(Hash *a) {
    if (a != NULL) {
        int i;
        for (i = 0; i < a->TABLE_SIZE; i++) {
            if (a->itens[i] != NULL)
                free(a->itens[i]);
        }
        free(a->itens);
        free(a);
    }
}


int main() {
    int s = 1024;
    Hash *table = criaHash(s);

    struct grupo gr, a[3] = {{1913, "Julia",   20, 54},
                             {7883, "Maria",   12, 42},
                             {5293, "Ana",     8,  21}};

    int i;
    for (i = 0; i < 3; i++) {
        insereHash(table, a[i]);
    }
    buscaHash(table, 1913, &gr);
    printf("%s, %d\n", gr.nome, gr.senha);
    buscaHash(table, 7883, &gr);
    printf("%s, %d\n", gr.nome, gr.senha);
    buscaHash(table, 5293, &gr);
    printf("%s, %d\n", gr.nome, gr.senha);

    removeHash(table);

    return 0;
}
